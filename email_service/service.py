from django.conf import settings
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from sendgrid import SendGridAPIClient, Mail

from core.utils import AccountActivationTokenGenerator


class EmailService:
    @staticmethod
    def send(*args, **kwargs):
        current_site = kwargs.get('current_site')
        user_id = kwargs.get('user_id')
        email = kwargs.get('email')
        account_activation_token = AccountActivationTokenGenerator()

        user = get_user_model().objects.get(id=user_id)

        mail_subject = 'Activate your iGPRO account.'
        message = render_to_string('after_register/email_activation.html', {
            'user': user,
            'domain': current_site,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })

        message = Mail(
            from_email=f'register@{settings.DOMAIN}',
            to_emails=email,
            subject=mail_subject,
            html_content=message)

        try:
            sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
            return f"Email sent to {email}"
        except Exception as e:
            print(e)
            raise e

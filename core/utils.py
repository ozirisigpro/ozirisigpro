import base64
import re

from Crypto.Cipher import AES

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
                six.text_type(user.pk) + six.text_type(timestamp) + six.text_type(user.is_active)
        )


class GPROCrypto:
    key = "20b420bb-76a6-4b".encode('utf-8')
    salt = "d0cd60c9-cbd3-45".encode('utf8')
    enc_dec_method = 'utf-8'

    @classmethod
    def encrypt_data(cls, str_to_enc):
        try:
            aes_obj = AES.new(cls.key, AES.MODE_CFB, cls.salt)
            hx_enc = aes_obj.encrypt(str_to_enc.encode('utf8'))
            mret = base64.b64encode(hx_enc).decode(cls.enc_dec_method)
            return mret
        except ValueError as value_error:
            if value_error.args[0] == 'IV must be 16 bytes long':
                raise ValueError('Encryption Error: SALT must be 16 characters long')
            elif value_error.args[0] == 'AES key must be either 16, 24, or 32 bytes long':
                raise ValueError('Encryption Error: Encryption key must be either 16, 24, or 32 characters long')
            else:
                raise ValueError(value_error)

    @classmethod
    def decrypt_data(cls, enc_str):
        try:
            aes_obj = AES.new(cls.key, AES.MODE_CFB, cls.salt)
            str_tmp = base64.b64decode(enc_str.encode(cls.enc_dec_method))
            str_dec = aes_obj.decrypt(str_tmp)
            mret = str_dec.decode(cls.enc_dec_method)
            return mret
        except ValueError as value_error:
            if value_error.args[0] == 'IV must be 16 bytes long':
                raise ValueError('Decryption Error: SALT must be 16 characters long')
            elif value_error.args[0] == 'AES key must be either 16, 24, or 32 bytes long':
                raise ValueError('Decryption Error: Encryption key must be either 16, 24, or 32 characters long')
            else:
                raise ValueError(value_error)


def convert_minute_to_secs(time_str):
    import re
    
    results = list(map(int, re.split(r'\D+', time_str)))
    if len(results) == 3:
        seconds = "%s.%s" % (
            results[0] * 60 + results[1],
            results[2]
        )
        return seconds
    else:
        return time_str

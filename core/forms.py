from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Div, HTML, Button
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.translation import gettext, gettext_lazy as _


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email_service address.')

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'signup-form'
        self.helper.form_method = 'post'
        self.helper.form_action = 'register'

        self.helper.layout = Layout(
            Field('username', ),
            Field('first_name', ),
            Field('last_name', ),
            Field('email', ),
            Field('password1', Div(HTML('<p class="form__password-strength" id="strength-output"></p>'), css_class='form_group mb-5'), css_class='form__item', id='password'),
            Field('password2', ),
        )

        self.helper.add_input(Submit('submit', 'Submit'))


class LoginForm(AuthenticationForm):
    class Meta:
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'})
        }

    def clean_username(self):
        username = self.data['username']
        if '@' in username:
            try:
                username = get_user_model().objects.get(email=username).username
            except ObjectDoesNotExist:
                raise ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
        return username


class GproAuthForm(forms.ModelForm):
    gpro_username = forms.CharField(
        label=_("GPRO Username"),
        strip=True,
        widget=forms.TextInput
    )
    gpro_password = forms.CharField(
        label=_("GPRO Password"),
        strip=False,
        widget=forms.PasswordInput
    )

    class Meta:
        model = get_user_model()
        fields = ('gpro_username', 'gpro_password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'gpro-auth-form'
        self.helper.form_method = 'post'
        self.helper.form_action = 'gpro-auth'

        send_button = Submit('submit', 'Submit')
        self.helper.add_input(send_button)

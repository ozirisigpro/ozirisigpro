from django.contrib.auth.models import AbstractUser
from django.db import models

from safedelete.models import SafeDeleteModel


class User(AbstractUser, SafeDeleteModel):
    gpro_username = models.CharField(max_length=255, blank=True, null=True)
    gpro_password = models.CharField(max_length=2000, blank=True, null=True)


class AuditMixin(models.Model):
    created_by = models.CharField(db_column='created_by', max_length=100, blank=True, editable=False, null=True)
    updated_by = models.CharField(db_column='updated_by', max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(db_column='created_at', auto_now_add=True)
    updated_at = models.DateTimeField(db_column='updated_at', auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        from core.middlewares import get_current_user
        user = get_current_user()
        if self.pk:
            self.updated_by = user and user.username or None
        else:
            self.created_by = user and user.username or None
        super(AuditMixin, self).save(*args, **kwargs)

from django.contrib.auth import get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render, render_to_response, redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.generic import FormView
from django.views.generic.base import View, TemplateView

from core.forms import SignUpForm, LoginForm
from core.utils import AccountActivationTokenGenerator
from tasks import EmailTask


class Login(LoginView):
    template_name = 'login/login.html'
    redirect_authenticated_user = True
    authentication_form = LoginForm


class Logout(LogoutView):
    next_page = reverse_lazy('login')


class SignUpFormView(FormView):
    form_class = SignUpForm
    template_name = 'register/register.html'

    def form_valid(self, form):
        """ What to do with the valid form ?
              - Create the user
              - Send a mail to confirm the email_service and the account creation

        """

        user = get_user_model().objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name'],
        )
        user.is_active = False
        user.save()

        args = []
        kwargs = {
            'user_id': user.id,
            'email': form.cleaned_data.get('email'),
            'current_site': self.request.META.get('HTTP_ORIGIN')
        }
        EmailTask.apply_async(args=args, kwargs=kwargs)

        return render_to_response("after_register/check_your_mail.html")


class AccountActivation(View):
    activate = None

    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = get_user_model().objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, get_user_model().DoesNotExist):
            user = None
        if self.activate:
            if user is not None and AccountActivationTokenGenerator().check_token(user, token):
                user.is_active = True
                user.save()
                login(request, user)
                return redirect('dashboard')
            else:
                return render(request, "after_register/invalid_activation_link.html")
        else:
            if user is not None and AccountActivationTokenGenerator().check_token(user, token):
                if not user.is_active:
                    user.delete()
                    user.save()
                    return render(request, "after_register/deactivate.html")
                else:
                    return render(request, "after_register/invalid_deactivation_link.html")
            else:
                return render(request, "after_register/invalid_deactivation_link.html")


def check_gpro_authentication(func):
    def wrapper(request):
        req_user = request.user
        if req_user.is_authenticated and req_user.is_active:
            user = get_user_model().objects.get(id=req_user.id)
            if user.gpro_username and user.gpro_password:
                return func(request)
            else:
                return redirect(reverse_lazy('gpro-auth'))
        else:
            return redirect(reverse_lazy('login'))
    return wrapper


def gpro_authentication(func):
    def wrapper(request):
        req_user = request.user
        if req_user.is_authenticated and req_user.is_active:
            user = get_user_model().objects.get(id=req_user.id)
            if user.gpro_username and user.gpro_password:
                return redirect(reverse_lazy('dashboard'))
            else:
                return func(request)
        else:
            return redirect(reverse_lazy('login'))
    return wrapper


@method_decorator(check_gpro_authentication, name='dispatch')
@method_decorator(login_required, name='dispatch')
class Dashboard(TemplateView):
    template_name = "dashboard/dashboard.html"

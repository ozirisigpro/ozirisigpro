import logging

import requests
from urllib3.util.retry import Retry

from core.exception import HttpError

logger = logging.getLogger(__name__)

__all__ = [
    'get',
    'post',
    '_requests_retry_session',
    'DEFAULT_RETRY',
    'CONNECT_RETRY',
    'NO_RETRY'
]

DEFAULT_RETRY = Retry(
    total=3,
    read=3,
    connect=3,
    backoff_factor=1,
    status_forcelist=(413, 429, 502, 503, 504),  # Retry.RETRY_AFTER_STATUS_CODES = frozenset([413, 429, 503])
    raise_on_status=False,
    respect_retry_after_header=False
)

CONNECT_RETRY = Retry(
    total=3,
    read=0,
    connect=3,
    backoff_factor=1,
    status_forcelist=(413, 429, 502, 503, 504),  # Retry.RETRY_AFTER_STATUS_CODES = frozenset([413, 429, 503])
    raise_on_status=False,
    respect_retry_after_header=False
)

NO_RETRY = 0


def get_subclass(cls, field_name, field_value):
    for subclass in cls.__subclasses__():
        if field_value == getattr(subclass, field_name, None):
            return subclass
        found = get_subclass(subclass, field_name, field_value)
        if found:
            return found


# https://www.peterbe.com/plog/best-practice-with-retries-with-requests
def _requests_retry_session(session=None, retry=DEFAULT_RETRY):
    from requests.adapters import HTTPAdapter
    session = session or requests.Session()
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def __handle_requests_exception(requests_exception):
    response = requests_exception.response

    error_cls: HttpError = get_subclass(HttpError, 'http_code', response.status_code)
    if error_cls:
        return error_cls(response=response)
    else:
        return HttpError(response=response)


def head(url, retry=DEFAULT_RETRY, **kwargs):
    session = _requests_retry_session(retry=retry)
    try:
        response = session.head(url, **kwargs)
        response.raise_for_status()
        return response
    except requests.exceptions.HTTPError as httpe:
        raise __handle_requests_exception(httpe)
    finally:
        session.close()


def get(url, retry=DEFAULT_RETRY, session=None, **kwargs):
    session = _requests_retry_session(session=session, retry=retry)
    try:
        response = session.get(url, **kwargs)
        response.raise_for_status()
        return response
    except requests.exceptions.HTTPError as httpe:
        raise __handle_requests_exception(httpe)
    finally:
        session.close()


def post(url, data=None, json=None, retry=DEFAULT_RETRY, session=None, **kwargs):
    session = _requests_retry_session(session=session, retry=retry)
    try:
        response = session.post(url, data=data, json=json, **kwargs)
        response.raise_for_status()
        return response
    except requests.exceptions.HTTPError as httpe:
        raise __handle_requests_exception(httpe)
    finally:
        session.close()

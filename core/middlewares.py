from threading import local

_locals = local()


def get_current_request():
    return get_variable('request', None)


def get_current_user():
    request = get_current_request()
    return getattr(request, 'user', None) if request else None


def set_variable(key, val):
    setattr(_locals, key, val)


def get_variable(key, default=None):
    return getattr(_locals, key, default)

from django.apps import AppConfig


class GproTrackConfig(AppConfig):
    name = 'gpro_track'
    label = 'gpro_track'

    def ready(self):
        pass

import threading
import logging

from tempfile import NamedTemporaryFile
from urllib.request import urlopen

from core.utils import convert_minute_to_secs
from django.conf import settings
from django.core.files import File as DjangoFile

from bs4 import BeautifulSoup
from django.utils.text import slugify

from gpro_track.models import Track

from core import http as request

logger = logging.getLogger(__name__)


class AllTrackService:
    url = settings.GPRO_ALL_TRACKS_URL
    souped_track_details_page = None

    @classmethod
    def get_site_response(cls):
        return request.get(cls.url)

    def parse(self, *args, **kwargs):
        threads = []

        resp = self.get_site_response()
        bs_content = BeautifulSoup(resp.content, "html.parser")
        tracks_table = bs_content.find("table")
        track_rows = tracks_table.find_all("tr")[1:]

        for track_row in track_rows:
            url_row = track_row.find_all("td")[0]
            track_url = url_row.find("a", href=True).get("href")
            self.parse_track_details(track_url)

    @staticmethod
    def get_track_details_page(track_url):
        url = f"{settings.GPRO_URL}{track_url}"
        return request.get(url)

    def soup_track_details_page(self, track_url):
        resp = self.get_track_details_page(track_url)
        self.souped_track_details_page = BeautifulSoup(resp.content, "html.parser")

    def parse_track_details(self, track_url):
        track_image = None
        self.soup_track_details_page(track_url)
        track_name = self.get_track_name()

        try:
            track = Track.objects.get(name=track_name)
        except Track.DoesNotExist:
            track = Track()
            track.name = track_name
        bs_content = self.souped_track_details_page
        data_tables = bs_content.find_all("table")
        for table in data_tables:
            tech_data_table = table.find(lambda tag: tag.name == "td" and "Location" in tag.text)
            if tech_data_table:
                track_image = DjangoFile(self.get_track_image(bs_content), name=f"{track.name}.gif")

                trs = table.find_all("tr")
                track.location = self.parse_tech_details(trs[0], "Location:", "text")
                track.power = int(self.parse_tech_details(trs[0], "Power:", "title"))
                track.handling = int(self.parse_tech_details(trs[1], "Handling:", "title"))
                track.race_distance = float(self.parse_tech_details(trs[2], "Race distance:", "text").replace("km", ""))
                track.acceleration = int(self.parse_tech_details(trs[2], "Acceleration:", "title"))
                track.laps = int(self.parse_tech_details(trs[3], "Laps:", "text"))
                track.downforce = Track.EXPRESSION.__getattr__(slugify(self.parse_tech_details(trs[3], "Downforce:", "text")))
                track.lap_distance = float(self.parse_tech_details(trs[4], "Lap distance:", "text").replace("\xa0km", ""))
                track.overtaking = Track.DIFFICULTY.__getattr__(slugify(self.parse_tech_details(trs[4], "Overtaking:", "text")))
                track.average_speed = float(self.parse_tech_details(trs[5], "Average speed:", "text").replace("\xa0km/h", ""))
                track.suspension = Track.PRESSURE.__getattr__(slugify(self.parse_tech_details(trs[5], "Suspension rigidity:", "text")))
                track.fuel = Track.EXPRESSION.__getattr__(slugify(self.parse_tech_details(trs[6], "Fuel consumption:", "text")))
                track.corners = int(self.parse_tech_details(trs[7], "Number of corners:", "text"))
                track.tyre = Track.EXPRESSION.__getattr__(slugify(self.parse_tech_details(trs[7], "Tyre wear:", "text")))
                track.pit_io = float(self.parse_tech_details(trs[8], "Time in/out of pits:", "text").replace("s", ""))
                track.grip = Track.INTENSITY.__getattr__(slugify(self.parse_tech_details(trs[8], "Grip level:", "text")))

            track_records_table = table.find(lambda tag: tag.name == "a" and "View best race laps of all managers" in tag.text)
            if track_records_table:
                trs = table.find_all("tr")
                track.elite_time = convert_minute_to_secs(self.parse_elite_time(trs[0]).replace("s", ""))

        if not track.image:
            track.image = track_image
        track.save()

    def get_track_name(self):
        bs_content = self.souped_track_details_page
        return bs_content.find("h1", attrs={"class": "block"}).text.strip()

    @staticmethod
    def parse_tech_details(tr, contains, attr):
        tds = tr.find_all("td")
        for td in tds:
            if contains in td:
                if attr == "text":
                    return td.find_next("td").text
                elif attr == "title":
                    return td.find_next("td").get("title")

    @staticmethod
    def parse_elite_time(tr):
        tds = tr.find_all("td")
        for td in tds:
            if any("View best race laps of all managers" in elem for elem in list(td.children)):
                record_tr = td.find_next("tr").find_next("tr")
                return record_tr.find_all("td")[-1].text

    @staticmethod
    def get_track_image(table):
        tds = table.find_all("td")
        for td in tds:
            track_image_url = td.find("img").get("src")
            if "images/tracks/" in track_image_url:
                img_temp = NamedTemporaryFile(delete=True)
                img_temp.write(urlopen(track_image_url).read())
                img_temp.flush()
                return img_temp

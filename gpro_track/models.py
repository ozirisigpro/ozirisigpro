from django.db import models
from django.utils.text import slugify

from model_utils import Choices

from core.models import AuditMixin


class Track(AuditMixin):
    class Meta:
        db_table = 'gpro_track'

    DIFFICULTY = Choices(
        (1, slugify("Very easy"), "Very easy"),
        (2, slugify("Easy"), "Easy"),
        (3, slugify("Normal"), "Normal"),
        (4, slugify("Hard"), "Hard"),
        (5, slugify("Very hard"), "Very hard")
    )
    EXPRESSION = Choices(
        (1, slugify("Very low"), "Very low"),
        (2, slugify("Low"), "Low"),
        (3, slugify("Medium"), "Medium"),
        (4, slugify("High"), "High"),
        (5, slugify("Very high"), "Very high")
    )
    INTENSITY = Choices(
        (1, slugify("Very low"), "Very low"),
        (2, slugify("Low"), "Low"),
        (3, slugify("Normal"), "Normal"),
        (4, slugify("High"), "High"),
        (5, slugify("Very high"), "Very high")
    )
    PRESSURE = Choices(
        (1, slugify("Soft"), "Soft"),
        (2, slugify("Medium"), "Medium"),
        (3, slugify("Hard"), "Hard")
    )

    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    race_distance = models.FloatField()
    laps = models.IntegerField()
    lap_distance = models.FloatField()
    average_speed = models.FloatField()
    corners = models.IntegerField()
    pit_io = models.FloatField()
    power = models.IntegerField()
    handling = models.IntegerField()
    acceleration = models.IntegerField()
    downforce = models.CharField(max_length=2, choices=EXPRESSION)
    overtaking = models.CharField(max_length=2, choices=DIFFICULTY)
    suspension = models.CharField(max_length=1, choices=PRESSURE)
    fuel = models.CharField(max_length=2, choices=EXPRESSION)
    tyre = models.CharField(max_length=2, choices=EXPRESSION)
    grip = models.CharField(max_length=2, choices=INTENSITY)
    elite_time = models.FloatField()
    image = models.ImageField(upload_to='images/tracks')

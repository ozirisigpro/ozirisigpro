from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'igpro.settings')

app = Celery("igpro-tasks")
app.config_from_object('django.conf:settings', namespace='CELERY')

app.Task.resultrepr_maxsize = 2048

app.conf.update(
    enable_utc=True,
    task_acks_late=True,
    worker_prefetch_multiplier=1,
    task_queue_max_priority=10,
    result_backend='django-db',
    task_ignore_result=True,
    task_store_errors_even_if_ignored=False,
    broker_connection_max_retries=0,
    broker_pool_limit=50,
    task_inherit_parent_priority=True,
    worker_hijack_root_logger=False,
)

app.conf.task_default_queue = 'celery'
app.autodiscover_tasks()

# if getattr(settings, 'ENABLE_QUEUE_ROUTING', False):
#     app.conf.task_routes = {
#         'domino_check_iana_root_extensions_task': {
#             'queue': 'collect.checkextensions'
#         },
#     }
# else:
#     app.conf.task_routes = {
#         '*': {
#             'queue': 'celery'
#         }
#     }
app.conf.task_routes = {
    '*': {
        'queue': 'celery'
    }
}

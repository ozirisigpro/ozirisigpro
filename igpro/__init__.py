from pkgutil import extend_path

from .celery import app as celery_app

__path__ = extend_path(__path__, __name__)

__all__ = ['celery_app']

from setuptools import setup, find_packages


setup(
    name="igpro",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="igpro",
    author="midhat",
    author_email="gpro.net",
    packages=find_packages(),
    zip_safe=True,
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3.6',
        'Topic :: iGpro'
    ],
    install_requires=[
        "Django==2.2.13",
        "django-crispy-forms==1.9.1",
        "celery==4.4.5",
        "django-environ==0.4.5",
        "sendgrid==6.3.1",
        "python-http-client==3.2.7",
        "tox==3.15.2",
        "django-nose==1.4.6",
        "gunicorn==20.0.4",
        "psycopg2-binary==2.8.5",
        "django-celery-beat==2.0.0",
        "django-celery-results==1.2.1",
        "django-safedelete==0.5.5",
        "requests==2.24.0",
        "beautifulsoup4==4.9.1",
        "pycrypto==2.6.1",
        "Pillow==7.2.0",
        "django-model-utils==4.0.0",
    ]
)

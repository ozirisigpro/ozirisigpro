import requests

from bs4 import BeautifulSoup
from celery import current_task
from django.conf import settings

from core.utils import GPROCrypto


class GproAuthService:
    url = settings.GPRO_URL
    login_url = settings.GPRO_LOGIN_URL

    @staticmethod
    def check_site_loaded(response):
        if response.status_code == 200:
            return True
        return False

    @staticmethod
    def check_successful(response):
        bs_content = BeautifulSoup(response.content, "html.parser")
        manager_info = bs_content.find("div", attrs={"class": "xmanagerinfo"})
        alert_element = bs_content.find("div", attrs={"class": "orange"})
        if manager_info:
            return True
        if alert_element:
            return False
        return False

    @classmethod
    def gpro_sign_in(cls, *args, **kwargs):
        gpro_username = kwargs.get('gpro_username')
        gpro_password = kwargs.get('gpro_password')
        decrypted_gpro_pass = GPROCrypto.decrypt_data(gpro_password)
        data = {"username": gpro_username,
                "password": decrypted_gpro_pass}
        with requests.Session() as s:
            status = "requested"
            current_task.update_state(
                state="PROGRESS",
                meta={
                    "status": status
                }
            )
            login_data = {"textLogin": data['username'], "textPassword": data['password'], "Logon": "Login"}
            headers = {
                'Accept-Language': 'en-US,en;q=0.9',
                'Origin': 'https://gpro.net',
                'Referer': 'https://gpro.net/gb/Login.asp?Redirect=gpro.asp',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36 OPR/69.0.3686.57'
            }
            resp = s.post(cls.login_url, login_data, headers=headers)

            if cls.check_site_loaded(resp):
                status = "ok"
                current_task.update_state(
                    state="PROGRESS",
                    meta={
                        "status": status
                    }
                )

            user_id = kwargs.get('user_id')

            if cls.check_successful(resp):
                status = "successful"
            else:
                status = "unsuccessful"
            current_task.update_state(
                state="FINISHED",
                meta={
                    "status": status,
                    "user_id": user_id,
                    "gpro_username": gpro_username,
                    "gpro_password": str(gpro_password)
                }
            )

            return {'status': status, 'user_id': user_id, 'gpro_username': gpro_username, 'gpro_password': gpro_password}

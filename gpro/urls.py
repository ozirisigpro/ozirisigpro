from django.urls import path

from gpro.views import GproAuthFormView, GproAuthViews


urlpatterns = [
    path('auth/', GproAuthFormView.as_view(), name='gpro-auth'),
    path('resp/', GproAuthViews.as_view(param='gpro-resp'), name='gpro-resp'),
    path('auth-info/', GproAuthViews.as_view(param='gpro-auth-info'), name='gpro-auth-info'),
    path('auth-validation/', GproAuthViews.as_view(param='gpro-auth-validation'),
         name='gpro-auth-validation'),
]

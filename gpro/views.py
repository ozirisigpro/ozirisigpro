import json

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View


from celery.result import AsyncResult
from django.views.generic import FormView

from core.forms import GproAuthForm
from core.utils import GPROCrypto
from core.views import gpro_authentication
from tasks.gpro_auth import GproAuthTask


@method_decorator(gpro_authentication, name='dispatch')
@method_decorator(login_required, name='dispatch')
class GproAuthFormView(FormView):
    form_class = GproAuthForm
    template_name = 'after_register/gpro_auth.html'

    def form_valid(self, form):
        req_user_id = self.request.user.id
        gpro_username = form.cleaned_data.get('gpro_username')
        gpro_password = form.cleaned_data.get('gpro_password')
        encryped_gpro_pass = GPROCrypto.encrypt_data(gpro_password)
        args = []
        kwargs = {
            "gpro_username": gpro_username,
            "gpro_password": encryped_gpro_pass,
            "user_id": req_user_id
        }
        task = GproAuthTask.apply_async(args=args, kwargs=kwargs)
        task_id = task.task_id
        return HttpResponseRedirect(reverse('gpro-resp') + '?task_id=' + task_id)


class GproAuthViews(View):
    param = None

    def get(self, request):
        if self.param == 'gpro-resp':
            return render(request, 'after_register/gpro_auth_loading.html', context={"task_id": request.GET.get("task_id")})
        elif self.param == 'gpro-auth-info':
            task_id = request.GET.get('task_id', None)
            if task_id is not None:
                task = AsyncResult(task_id)
                data = {
                    'state': task.state,
                    'result': task.result,
                }
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                return HttpResponse('No job id given.')
        elif self.param == 'gpro-auth-validation':
            task_id = request.GET.get('task_id', None)
            if task_id is not None:
                task = AsyncResult(task_id)
                if task.state == 'FINISHED' and task.result.get('status') == 'successful':
                    gpro_username = task.result.get('gpro_username')
                    gpro_password = task.result.get('gpro_password')
                    user_id = int(task.result.get('user_id'))
                    user = get_user_model().objects.get(id=user_id)
                    user.gpro_username = gpro_username
                    user.gpro_password = gpro_password
                    user.save()
                    return HttpResponseRedirect(reverse('dashboard'))
                else:
                    messages.error(request, "Wrong credentials!")
                    return HttpResponseRedirect(reverse('gpro-auth'))
            else:
                return HttpResponseRedirect(reverse('gpro-auth'))

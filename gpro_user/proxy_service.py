import logging
from typing import Iterator

from django.conf import settings
from django.contrib.auth import get_user_model

from core import http as request
from requests.exceptions import HTTPError, ConnectTimeout, ProxyError

from core.http import NO_RETRY
from gpro_user.models import UsedIP

logger = logging.getLogger(__name__)


class ProxyService:
    url = 'https://api.proxyscrape.com/'
    test_ip_url = 'https://api.myip.com/'
    country = 'all'
    timeout = 10000

    user_model = get_user_model()

    used_ips = UsedIP.objects.values_list('ip_address', flat=True)

    def prepare_request_filter(self, country, timeout):
        url = f'{self.url}?request=displayproxies&proxytype=http&timeout={timeout}&country={country}&ssl=yes&anonymity=elite'
        return request.get(url)

    def prepare_request_all(self):
        url = f'{self.url}?request=displayproxies&proxytype=http&timeout=10000&country=all&ssl=yes&anonymity=elite'
        return request.get(url)

    def select_ip(self, ip_addresses: Iterator[str], random=False):
        logger.info(f'Selecting ip addresses')
        while True:
            try:
                ip_address = next(ip_addresses)
                logger.info(f'IP address selected: {ip_address}')
            except StopIteration:
                ip_address = self.select_random_ip()
            try:
                check_ip_resp = request.get(self.test_ip_url, retry=NO_RETRY, proxies={'http': f'http://{ip_address}', 'https': f'https://{ip_address}'}, timeout=(self.timeout/1000))
            except (HTTPError, ConnectTimeout, ProxyError):
                continue
            logger.info(f'Checking myapi for ip_address {ip_address}')
            if check_ip_resp.status_code == 200 and check_ip_resp.json().get('ip') != settings.SERVER_IP:
                logger.info(f'Response successful for ip {ip_address}')
                if random:
                    logger.warning(f'Random is enabled')
                    if ip_address in self.used_ips:
                        logger.debug(f'Used IP address')
                        pass
                else:
                    logger.info(f'IP address selecting SUCCESS {ip_address}')
                    return ip_address

    def find_ip_address(self, resp, random=False):
        if resp.status_code == 200:
            logger.info(f'Proxyscrape response successful')
            ips = iter(list(filter(None, resp.text.split('\r\n'))))
            ip_address = self.select_ip(ips, random)
            return ip_address

    def select_random_ip(self):
        logger.warning(f'Randomizing ip addresses')
        resp = self.prepare_request_all()
        return self.find_ip_address(resp, random=True)

    def get_ip(self, country, timeout):
        logger.info(f'Getting ip address')
        resp = self.prepare_request_filter(country, timeout)
        return self.find_ip_address(resp)

    def set_user_proxy_ip(self, *args, **kwargs):
        self.country = kwargs.get('country')
        logger.info(f'Getting ip address for country: {self.country}')
        ip_address = self.get_ip(self.country, self.timeout)

        logger.info(f'Ip is {ip_address}')
        user_id = kwargs.get('user_id')
        user = self.user_model.objects.get(id=user_id)
        UsedIP.objects.create(user=user, ip_address=ip_address)

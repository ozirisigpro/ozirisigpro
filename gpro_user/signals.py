from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.db.transaction import atomic
from django.dispatch import receiver

from gpro_user.models import UserProxySettings, ProxyCountry
from tasks import ProxyIPTask

user_model = get_user_model()


@receiver(post_save, sender=user_model)
def add_user_proxy_settings_signal(sender, instance, created, **kwargs):
    add_proxy_settings(created, instance)


@atomic
def add_proxy_settings(created, instance):
    if created:
        country = ProxyCountry.objects.filter(used=False).first()
        UserProxySettings.objects.create(user=instance, proxy_country=country)
        country.used = True
        country.save()

        kwargs = {
            'user_id': instance.id,
            'country': country.country,
        }
        ProxyIPTask.apply_async(kwargs=kwargs)

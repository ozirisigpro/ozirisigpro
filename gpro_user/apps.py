from django.apps import AppConfig


class GproUserConfig(AppConfig):
    name = 'gpro_user'
    label = 'gpro_user'

    def ready(self):
        import gpro_user.signals  # noqa

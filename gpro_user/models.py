from django.contrib.auth import get_user_model
from django.db import models

user_model = get_user_model()


class ProxyCountry(models.Model):
    class Meta:
        db_table = "gpro_user_proxy_country"

    country = models.CharField(max_length=2)
    used = models.BooleanField(default=False)


class UserProxySettings(models.Model):
    class Meta:
        db_table = "gpro_user_proxy_settings"

    user = models.OneToOneField(user_model, on_delete=models.CASCADE, blank=False, null=False)
    proxy_country = models.ForeignKey(ProxyCountry, on_delete=models.CASCADE, blank=False, null=False)


class UsedIP(models.Model):
    class Meta:
        db_table = "gpro_user_used_ip"

    user = models.ForeignKey(user_model, on_delete=models.CASCADE, blank=False, null=False)
    ip_address = models.CharField(max_length=255)

import logging

from email_service.service import EmailService
from igpro.celery import app
from tasks import BaseTask

logger = logging.getLogger(__name__)


class EmailTask(BaseTask):
    name = 'igpro_email_task'

    def execute(self, *args, **kwargs):
        return EmailService.send(*args, **kwargs)


EmailTask = app.register_task(EmailTask())

import logging
from celery import Task
from django.conf import settings

logger = logging.getLogger(__name__)

__all__ = ['BaseTask', 'EmailTask', 'ProxyIPTask', 'GproTracksTask']


class BaseTask(Task):
    name = None
    callback = None

    def execute(self, *args, **kwargs):
        raise NotImplementedError

    def run(self, *args, **kwargs):
        local_params = locals()
        logger.info(f"'{self.name}' with params '{local_params}' is started")

        result = self.execute(*args, **kwargs)

        logger.info(f"{self.name} with params '{local_params}' is finished")

        if settings.DEBUG:
            logger.debug(f'Task: {self.name}, Context: {local_params}, Result: {result}')

        return result

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.exception(exc)
        # raise exc  # NOTE: django-celery-results is not working properly in case of 'raise'


from .email import EmailTask  # noqa
from .proxy_ip import ProxyIPTask  # noqa
from .gpro_tracks import GproTracksTask  # noqa

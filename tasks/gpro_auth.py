import logging

from gpro.service import GproAuthService
from igpro.celery import app
from tasks import BaseTask

logger = logging.getLogger(__name__)


class GproAuthTask(BaseTask):
    name = 'igpro_auth_task'

    def execute(self, *args, **kwargs):
        return GproAuthService().gpro_sign_in(*args, **kwargs)


GproAuthTask = app.register_task(GproAuthTask())

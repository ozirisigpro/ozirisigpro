import logging

from gpro_user.proxy_service import ProxyService
from igpro.celery import app
from tasks import BaseTask

logger = logging.getLogger(__name__)


class ProxyIPTask(BaseTask):
    name = 'igpro_proxy_ip_task'

    def execute(self, *args, **kwargs):
        return ProxyService().set_user_proxy_ip(*args, **kwargs)


ProxyIPTask = app.register_task(ProxyIPTask())

import logging

from gpro_track.service import AllTrackService

from igpro.celery import app
from tasks import BaseTask

logger = logging.getLogger(__name__)


class GproTracksTask(BaseTask):
    name = 'igpro_gpro_tracks_task'

    def execute(self, *args, **kwargs):
        return AllTrackService().parse(*args, **kwargs)


GproTracksTask = app.register_task(GproTracksTask())
